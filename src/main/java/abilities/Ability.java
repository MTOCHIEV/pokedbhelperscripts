package pokedbutils.abilities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Ability {
    String id;
    String name;
}
