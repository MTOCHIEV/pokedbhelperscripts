package pokedbutils.abilities;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import pokedbutils.pokedexcolors.MissingPokedexColors;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {

    public static void main(String[] args) {
        List<List<String>> abilities = convert_csv_to_entry_list("abilities.csv");
        List<List<String>> pokemon_abilities = convert_csv_to_entry_list("pokemon_abilities.csv");


        List<Ability> abilities_list = abilities.stream().map(entry -> new Ability(entry.get(0), entry.get(2))).toList();

        List<Pokemon_Ability> pokemonAbilities = pokemon_abilities.stream().map(entry -> {

            Pokemon_Ability pokemon_ability = new Pokemon_Ability();
            pokemon_ability.setAbility1(entry.get(0));
            pokemon_ability.setAbility2(entry.get(1));
            pokemon_ability.setHidden_ability(entry.get(2));
            abilities_list.stream().filter(ability -> entry.get(0).equalsIgnoreCase(ability.name)).findFirst().ifPresent(ability -> {
                pokemon_ability.setAbility1(ability.id);
            });
            abilities_list.stream().filter(ability -> entry.get(1).equalsIgnoreCase(ability.name)).findFirst().ifPresent(ability -> {
                pokemon_ability.setAbility2(ability.id);
            });
            abilities_list.stream().filter(ability -> entry.get(2).equalsIgnoreCase(ability.name)).findFirst().ifPresent(ability -> {
                pokemon_ability.setHidden_ability(ability.id);
            });
            return pokemon_ability;
        }).toList();

        for (Pokemon_Ability pokemonAbility : pokemonAbilities) {
            System.out.println(pokemonAbility.ability1 + ";" + pokemonAbility.ability2 + ";" + pokemonAbility.hidden_ability);
        }
    }


    public static List<List<String>> convert_csv_to_entry_list(String csvPath) {
        URL csvURL = MissingPokedexColors.class.getClassLoader().getResource(csvPath);
        List<List<String>> records = new ArrayList<>();
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvURL.toURI()));
        } catch (IOException | URISyntaxException e) {
            System.out.println("COULD NOT READ CSV FILE");
            throw new RuntimeException(e);
        }
        CSVParser parser = new CSVParserBuilder().withSeparator(';').withIgnoreQuotations(true).build();
        CSVReader csv_reader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();
        String[] values;
        while (true) {
            try {
                if ((values = csv_reader.readNext()) == null) break;
            } catch (IOException | CsvValidationException e) {
                System.out.println("COULD NOT READ NEXT CSV LINE");
                throw new RuntimeException(e);
            }
            records.add(Arrays.asList(values));
        }
        return records;
    }
}
