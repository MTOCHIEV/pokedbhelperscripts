package pokedbutils.abilities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pokemon_Ability {
    String ability1;
    String ability2;
    String hidden_ability;
}
