package pokedbutils.ability_prose;

import pokedbutils.move_conversion.Utils;

import java.util.List;

public class App {
    public static void main(String[] args) {
        List<CSV_Ability_Prose> csv_ability_proses = Utils.convert_csv_to_entry_list(CSV_Ability_Prose.class, "ability_prose.csv");

        csv_ability_proses.forEach(entry -> {
            String[] splitparts = entry.getShort_effect().split("((?=\\[)|(?<=\\}))");
            int id = 0;
            for (String splitpart : splitparts) {
                id++;
                String text;
                String icon;
                String is_link;
                String link_target;
                String hex_color;
                if (splitpart.equalsIgnoreCase("[accuracy]{mechanic:accuracy}")) {
                    text = "accuracy";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_accuracy.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Attack]{mechanic:attack}")) {
                    text = "Attack";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_attack.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burned]{mechanic:burn}")) {
                    text = "burned";
                    icon = "pokemon_data/assets/ability_prose_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burning]{mechanic:burn}")) {
                    text = "burning";
                    icon = "pokemon_data/assets/ability_prose_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burns]{mechanic:burn}")) {
                    text = "burns";
                    icon = "pokemon_data/assets/ability_prose_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[confused]{mechanic:confusion}")) {
                    text = "confused";
                    icon = "pokemon_data/assets/ability_prose_icons/status_confusion.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[confusion]{mechanic:confusion}")) {
                    text = "confusion";
                    icon = "pokemon_data/assets/ability_prose_icons/status_confusion.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[critical hits]{mechanic:critical-hit}")) {
                    text = "critical hits";
                    icon = "pokemon_data/assets/ability_prose_icons/critical_hit.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[critical hit]{mechanic:critical-hit}")) {
                    text = "critical hit";
                    icon = "pokemon_data/assets/ability_prose_icons/critical_hit.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Defense]{mechanic:defense}")) {
                    text = "Defense";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_defense.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Disabling]{move:disable}")) {
                    text = "Disabling";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move?move=disable";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[evasion]{mechanic:evasion}")) {
                    text = "evasion";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_evasion_up.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[flinching]{mechanic:flinching}")) {
                    text = "flinching";
                    icon = "pokemon_data/assets/ability_prose_icons/status_flinch.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[flinch]{mechanic:flinch}")) {
                    text = "flinch";
                    icon = "pokemon_data/assets/ability_prose_icons/status_flinch.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Flung]{move:fling}")) {
                    text = "Flung";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=fling";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[freezing]{mechanic:freezing}")) {
                    text = "freezing";
                    icon = "pokemon_data/assets/ability_prose_icons/status_frozen.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[hail]{mechanic:hail}")) {
                    text = "hail";
                    icon = "pokemon_data/assets/ability_prose_icons/weather_blizzard.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[HP]{mechanic:hp}")) {
                    text = "HP";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_hp.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[infatuating]{mechanic:infatuation}")) {
                    text = "infatuating";
                    icon = "pokemon_data/assets/ability_prose_icons/status_infatuated.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "";
                } else if (splitpart.equalsIgnoreCase("[infatuation]{mechanic:infatuation}")) {
                    text = "infatuation";
                    icon = "pokemon_data/assets/ability_prose_icons/status_infatuated.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[lowered]{mechanic:stat-modifiers}")) {
                    text = "lowered";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[lowered]{mechanic:stat-modifier}")) {
                    text = "lowered";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[major status ailments]{mechanic:major-status-ailments}")) {
                    text = "major status ailments";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[major status ailment]{mechanic:major-status-ailment}")) {
                    text = "major status ailment";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[not-very-effective]{mechanic:not-very-effective}")) {
                    text = "not-very-effective";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[paralysis]{mechanic:paralysis}")) {
                    text = "paralysis";
                    icon = "pokemon_data/assets/ability_prose_icons/status_paralysis.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[paralyzing]{mechanic:paralysis}")) {
                    text = "paralyzing";
                    icon = "pokemon_data/assets/ability_prose_icons/status_paralysis.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[poisoned]{mechanic:poison}")) {
                    text = "poisoned";
                    icon = "pokemon_data/assets/ability_prose_icons/status_poison.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[poisoning]{mechanic:poison}")) {
                    text = "poisoning";
                    icon = "pokemon_data/assets/ability_prose_icons/status_poison.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[poison]{mechanic:poison}")) {
                    text = "poison";
                    icon = "pokemon_data/assets/ability_prose_icons/status_poison.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[PP]{mechanic:pp}")) {
                    text = "PP";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[rain]{mechanic:rain}")) {
                    text = "rain";
                    icon = "pokemon_data/assets/ability_prose_icons/weather_rain.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[same-type attack bonus]{mechanic:same-type-attack-bonus}")) {
                    text = "same-type attack bonus";
                    icon = "pokemon_data/assets/ability_prose_icons/stab_bonus.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[sandstorm]{mechanic:sandstorm}")) {
                    text = "sandstorm";
                    icon = "pokemon_data/assets/ability_prose_icons/weather_sandstorm.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[sleeping]{mechanic:sleep}")) {
                    text = "sleeping";
                    icon = "pokemon_data/assets/ability_prose_icons/status_sleep.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[sleep]{mechanic:sleep}")) {
                    text = "sleep";
                    icon = "pokemon_data/assets/ability_prose_icons/status_sleep.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Special Attack]{mechanic:special-attack}")) {
                    text = "Special Attack";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_spatk.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Special Defense]{mechanic:special-defense}")) {
                    text = "Special Defense";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_spdef.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Speed]{mechanic:speed}")) {
                    text = "Speed";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_speed.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stages]{mechanic:stat-modifier}")) {
                    text = "stages";
                    icon = "-";
                    is_link = "-";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stage]{mechanic:stat-modifier}")) {
                    text = "stage";
                    icon = "-";
                    is_link = "-";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stat changes]{mechanic:stat-modifiers}")) {
                    text = "stat changes";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_stats.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stat modifiers]{mechanic:stat-modifiers}")) {
                    text = "stat modifiers";
                    icon = "pokemon_data/assets/ability_prose_icons/stat_stats.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[strong sunlight]{mechanic:strong-sunlight}")) {
                    text = "strong sunlight";
                    icon = "pokemon_data/assets/ability_prose_icons/weather_harsh_sunlight.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[super effective]{mechanic:super-effective}")) {
                    text = "super-effective";
                    icon = "pokemon_data/assets/ability_prose_icons/super-effective.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[super-effective]{mechanic:super-effective}")) {
                    text = "super-effective";
                    icon = "pokemon_data/assets/ability_prose_icons/super-effective.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Transforms]{move:transform}")) {
                    text = "Transforms";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=transform";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[weather]{mechanic:weather}")) {
                    text = "weather";
                    icon = "pokemon_data/assets/ability_prose_icons/weather_weather.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:aftermath}")) {
                    text = "Aftermath";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=aftermath";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:dark-aura}")) {
                    text = "Dark Aura";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=dark-aura";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:fairy-aura}")) {
                    text = "Fairy Aura";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=fairy-aura";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:minus}")) {
                    text = "Minus";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=minus";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:plus}")) {
                    text = "Plus";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=plus";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{item:honey}")) {
                    text = "Honey";
                    icon = "pokemon_data/assets/ability_prose_icons/honey.png";
                    is_link = "TRUE";
                    link_target = "/item.xhtml?item=honey";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:captivate}")) {
                    text = "Captivate";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=captivate";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:explosion}")) {
                    text = "Explosion";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=explosion";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:grassy-terrain}")) {
                    text = "Grassy Terrain";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=grassy-terrain";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:kings-shield}")) {
                    text = "King's Shield";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=kings-shield";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:light-screen}")) {
                    text = "Light Screen";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=light-screen";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:reflect}")) {
                    text = "Reflect";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=reflect";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:safeguard}")) {
                    text = "Safeguard";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=safeguard";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:self-destruct}")) {
                    text = "Self Destruct";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=self-destruct";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:triple-kick}")) {
                    text = "Triple Kick";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=triple-kick";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:aegislash}")) {
                    text = "Aegislash";
                    icon = "pokemon_data/assets/ability_prose_icons/aegislash.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=aegislash";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:arceus}")) {
                    text = "Arceus";
                    icon = "pokemon_data/assets/ability_prose_icons/arceus.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=arceus";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:castform}")) {
                    text = "Castform";
                    icon = "pokemon_data/assets/ability_prose_icons/castform.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=castform";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:darmanitan}")) {
                    text = "Darmanitan";
                    icon = "pokemon_data/assets/ability_prose_icons/darmanitan.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=darmanitan";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{type:bug}")) {
                    text = "BUG";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_bug_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=bug";
                    hex_color = "#95A235";
                } else if (splitpart.equalsIgnoreCase("[]{type:dark}")) {
                    text = "DARK";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_dark_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=dark";
                    hex_color = "#705848";
                } else if (splitpart.equalsIgnoreCase("[]{type:electric}")) {
                    text = "ELECTRIC";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_electric_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=electric";
                    hex_color = "#F2C341";
                } else if (splitpart.equalsIgnoreCase("[]{type:fairy}")) {
                    text = "FAIRY";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_fairy_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=fairy";
                    hex_color = "#74A3BA";
                } else if (splitpart.equalsIgnoreCase("[]{type:fighting}")) {
                    text = "FIGHTING";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_fighting_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=fighting";
                    hex_color = "#F08832";
                } else if (splitpart.equalsIgnoreCase("[]{type:fire}")) {
                    text = "FIRE";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_fire_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=fire";
                    hex_color = "#D33A30";
                } else if (splitpart.equalsIgnoreCase("[]{type:flying}")) {
                    text = "FLYING";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_flying_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=flying";
                    hex_color = "#8FB8E5";
                } else if (splitpart.equalsIgnoreCase("[]{type:ghost}")) {
                    text = "GHOST";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_ghost_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=ghost";
                    hex_color = "#6C436E";
                } else if (splitpart.equalsIgnoreCase("[]{type:grass}")) {
                    text = "GRASS";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_grass_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=grass";
                    hex_color = "#5D9D3C";
                } else if (splitpart.equalsIgnoreCase("[]{type:ground}")) {
                    text = "GROUND";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_ground_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=ground";
                    hex_color = "#89532A";
                } else if (splitpart.equalsIgnoreCase("[]{type:ice}")) {
                    text = "ICE";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_ice_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=ice";
                    hex_color = "#79CCEF";
                } else if (splitpart.equalsIgnoreCase("[]{type:normal}")) {
                    text = "NORMAL";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_normal_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=normal";
                    hex_color = "#A1A1A1";
                } else if (splitpart.equalsIgnoreCase("[]{type:rock}")) {
                    text = "ROCK";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_rock_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=rock";
                    hex_color = "#ADAA84";
                } else if (splitpart.equalsIgnoreCase("[]{type:steel}")) {
                    text = "STEEL";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_steel_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=steel";
                    hex_color = "#4D3F3F";
                } else if (splitpart.equalsIgnoreCase("[]{type:water}")) {
                    text = "WATER";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_water_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=water";
                    hex_color = "#4D79BC";
                } else {
                    text = splitpart;
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                }

                System.out.println(entry.getAbility_id() + ";" + id + ";" + text + ";" + icon + ";" + is_link + ";" + link_target + ";" + hex_color);
            }
        });
    }
}
