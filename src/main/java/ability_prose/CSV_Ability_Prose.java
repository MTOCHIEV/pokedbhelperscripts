package pokedbutils.ability_prose;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"ability_id", "short_effect", "effect"})
public class CSV_Ability_Prose {

    private int ability_id;
    private String effect;
    private String short_effect;
}