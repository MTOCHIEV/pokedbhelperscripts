package pokedbutils.csvcleaner;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class CSVCleaner {

    public static void main(String[] args) throws URISyntaxException {
        URL url_source = CSVCleaner.class.getClassLoader().getResource("pokemon_data/csv/pokeapi");
        File directory_source = new File(url_source.toURI());
        new File("pokemon_data/csv/parsed").mkdirs();
        for (File file : directory_source.listFiles((dir, name) -> name.endsWith(".csv"))) {
            List<List<String>> csvData = DB_Startup_Utils.convert_csv_to_entry_list_apache(file.getAbsolutePath());
            List<List<String>> abilities_csv = csvData.stream().map(entry -> entry.stream().map(s -> s.replace("\n", " ").replace("  ", " ")).toList()).toList();
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath().replace("\\pokeapi\\", "\\parsed\\")));
                writer.write('\ufeff');
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(';'));
                for (List<String> row : abilities_csv) {
                    csvPrinter.printRecord(row);
                }
                csvPrinter.flush();
                csvPrinter.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Could not create CSVPrinter while reformatting CSV files");
            }
        }
    }

}
