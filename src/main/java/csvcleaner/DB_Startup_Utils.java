package pokedbutils.csvcleaner;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DB_Startup_Utils {
    public static final String format = "%1$-50s%2$-50s\n";

    public static List<List<String>> convert_csv_to_entry_list(String csv_path) {
        URL csv_url = DB_Startup_Utils.class.getClassLoader().getResource(csv_path);
        List<List<String>> records = new ArrayList<>();
        Reader reader;
        try {
            reader = Files.newBufferedReader(Paths.get(csv_url.toURI()));
        } catch (IOException | URISyntaxException e) {
            System.out.println("COULD NOT READ CSV FILE");
            throw new RuntimeException(e);
        }
        CSVParser parser = new CSVParserBuilder().withSeparator(';').withIgnoreQuotations(false).build();
        CSVReader csv_reader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();
        String[] values;
        while (true) {
            try {
                if ((values = csv_reader.readNext()) == null) break;
            } catch (IOException | CsvValidationException e) {
                System.out.println("COULD NOT READ NEXT CSV LINE");
                throw new RuntimeException(e);
            }
            records.add(Arrays.asList(values));
        }
        return records;
    }

    @SneakyThrows
    public static List<List<String>> convert_csv_to_entry_list_apache(String csv_path) {
        URI csv_uri = new File(csv_path).toURI();
        Reader in = Files.newBufferedReader(Paths.get(csv_uri));
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder().setSkipHeaderRecord(false).build();
        Iterable<CSVRecord> records = csvFormat.parse(in);
        List<List<String>> output = new ArrayList<>();
        records.forEach(strings -> {
            output.add(strings.stream().toList());
        });
        return output;
    }
}
