package pokedbutils.move_conversion;

import lombok.SneakyThrows;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {

        List<PokeApi_Move> pokeApi_moves_csv = Utils.convert_csv_to_entry_list(PokeApi_Move.class, "move_conversion/moves.csv");
        List<PokeApi_Pokemon> pokeApi_pokemon_csv = Utils.convert_csv_to_entry_list(PokeApi_Pokemon.class, "move_conversion/pokemon.csv");
        List<PokeApi_Pokemon_Move> pokeApi_pokemon_move_csv = Utils.convert_csv_to_entry_list(PokeApi_Pokemon_Move.class, "move_conversion/pokemon_moves.csv");
        List<PokeApi_Pokemon_Move_Method> pokeApi_pokemon_move_method_csv = Utils.convert_csv_to_entry_list(PokeApi_Pokemon_Move_Method.class, "move_conversion/pokemon_move_methods.csv");
        List<PokeApi_Pokemon_Move_Method_Prose> pokeApi_pokemon_move_method_prose_csv = Utils.convert_csv_to_entry_list(PokeApi_Pokemon_Move_Method_Prose.class, "move_conversion/pokemon_move_method_prose.csv");
        List<Custom_Pokemon_Form> custom_pokemon_form_csv = Utils.convert_csv_to_entry_list(Custom_Pokemon_Form.class, "move_conversion/pokemon_forms.csv");
        List<Custom_Pokemon_Name> custom_pokemon_name_csv = Utils.convert_csv_to_entry_list(Custom_Pokemon_Name.class, "move_conversion/pokemon_names.csv");

        for (Custom_Pokemon_Form custom_pokemon_form : custom_pokemon_form_csv) {
            for (Custom_Pokemon_Name custom_pokemon_name : custom_pokemon_name_csv) {
                if (custom_pokemon_form.getNat_dex_id() == custom_pokemon_name.getNdex_id()) {
                    custom_pokemon_form.setName(custom_pokemon_name.getName_english());
                }
            }
        }

        for (PokeApi_Pokemon pokeApi_pokemon : pokeApi_pokemon_csv) {
            pokeApi_pokemon.setNew_id("NEED TO FIX: " + pokeApi_pokemon.getIdentifier());

            for (Custom_Pokemon_Form custom_pokemon_form : custom_pokemon_form_csv) {
                if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase(custom_pokemon_form.getName()) &&
                    custom_pokemon_form.getForm_name().equalsIgnoreCase("Normal Form")) {
                    pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                } else if (pokeApi_pokemon.getIdentifier().contains("-mega") && custom_pokemon_form.getForm_name().contains("Mega ")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-mega", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().contains("-hisui") && custom_pokemon_form.getForm_name().contains("Hisui")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-hisui", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().contains("-alola") && custom_pokemon_form.getForm_name().contains("Alola")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-alola", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().contains("-galar") && custom_pokemon_form.getForm_name().contains("Galar")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-galar", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().contains("-gmax") && custom_pokemon_form.getForm_name().contains("Gigantamax")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-gmax", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().contains("-male") && custom_pokemon_form.getForm_name().contains("Male")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-male", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().contains("-female") && custom_pokemon_form.getForm_name().contains("Female")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-female", "").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("type-null")) {
                    pokeApi_pokemon.setNew_id("986");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("aegislash-blade")) {
                    pokeApi_pokemon.setNew_id("873");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("aegislash-shield")) {
                    pokeApi_pokemon.setNew_id("872");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("alcremie")) {
                    pokeApi_pokemon.setNew_id("1103");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("araquanid-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("basculin-blue-striped")) {
                    pokeApi_pokemon.setNew_id("670");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("basculin-red-striped")) {
                    pokeApi_pokemon.setNew_id("669");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("basculin-white-striped")) {
                    pokeApi_pokemon.setNew_id("671");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("burmy")) {
                    pokeApi_pokemon.setNew_id("507");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("calyrex-ice")) {
                    pokeApi_pokemon.setNew_id("1141");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("calyrex-shadow")) {
                    pokeApi_pokemon.setNew_id("1140");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("castform-rainy")) {
                    pokeApi_pokemon.setNew_id("432");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("castform-snowy")) {
                    pokeApi_pokemon.setNew_id("433");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("castform-sunny")) {
                    pokeApi_pokemon.setNew_id("431");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("charizard-mega-x")) {
                    pokeApi_pokemon.setNew_id("9");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("charizard-mega-y")) {
                    pokeApi_pokemon.setNew_id("10");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("cherrim")) {
                    pokeApi_pokemon.setNew_id("520");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("cramorant-gorging")) {
                    pokeApi_pokemon.setNew_id("1078");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("cramorant-gulping")) {
                    pokeApi_pokemon.setNew_id("1077");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("darmanitan-galar-standard")) {
                    pokeApi_pokemon.setNew_id("679");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("darmanitan-galar-zen")) {
                    pokeApi_pokemon.setNew_id("680");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("darmanitan-standard")) {
                    pokeApi_pokemon.setNew_id("677");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("darmanitan-zen")) {
                    pokeApi_pokemon.setNew_id("678");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("deerling")) {
                    pokeApi_pokemon.setNew_id("713");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("deoxys-attack")) {
                    pokeApi_pokemon.setNew_id("479");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("deoxys-defense")) {
                    pokeApi_pokemon.setNew_id("480");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("deoxys-normal")) {
                    pokeApi_pokemon.setNew_id("478");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("deoxys-speed")) {
                    pokeApi_pokemon.setNew_id("481");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("dialga-origin")) {
                    pokeApi_pokemon.setNew_id("596");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("dudunsparce")) {
                    pokeApi_pokemon.setNew_id("1236");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("dudunsparce-three-segment")) {
                    pokeApi_pokemon.setNew_id("1237");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("eevee-starter")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("eiscue-ice")) {
                    pokeApi_pokemon.setNew_id("1109");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("eiscue-noice")) {
                    pokeApi_pokemon.setNew_id("1110");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("enamorus-incarnate")) {
                    pokeApi_pokemon.setNew_id("1150");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("enamorus-therian")) {
                    pokeApi_pokemon.setNew_id("1151");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("eternatus-eternamax")) {
                    pokeApi_pokemon.setNew_id("1130");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("farfetchd")) {
                    pokeApi_pokemon.setNew_id("114");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("farfetchd-galar")) {
                    pokeApi_pokemon.setNew_id("115");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("flabebe")) {
                    pokeApi_pokemon.setNew_id("837");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("floette")) {
                    pokeApi_pokemon.setNew_id("842");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("floette-eternal")) {
                    pokeApi_pokemon.setNew_id("848");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("florges")) {
                    pokeApi_pokemon.setNew_id("849");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("frillish")) {
                    pokeApi_pokemon.setNew_id("726");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("furfrou")) {
                    pokeApi_pokemon.setNew_id("857");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gastrodon")) {
                    pokeApi_pokemon.setNew_id("524");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gimmighoul")) {
                    pokeApi_pokemon.setNew_id("1254");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gimmighoul-roaming")) {
                    pokeApi_pokemon.setNew_id("1255");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("giratina-altered")) {
                    pokeApi_pokemon.setNew_id("601");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("giratina-origin")) {
                    pokeApi_pokemon.setNew_id("602");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gourgeist-average")) {
                    pokeApi_pokemon.setNew_id("909");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gourgeist-large")) {
                    pokeApi_pokemon.setNew_id("910");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gourgeist-small")) {
                    pokeApi_pokemon.setNew_id("908");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gourgeist-super")) {
                    pokeApi_pokemon.setNew_id("911");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("greninja-ash")) {
                    pokeApi_pokemon.setNew_id("807");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("greninja-battle-bond")) {
                    pokeApi_pokemon.setNew_id("806");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("groudon-primal")) {
                    pokeApi_pokemon.setNew_id("474");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("gumshoos-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("hoopa")) {
                    pokeApi_pokemon.setNew_id("925");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("hoopa-unbound")) {
                    pokeApi_pokemon.setNew_id("926");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("jellicent")) {
                    pokeApi_pokemon.setNew_id("727");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("keldeo-ordinary")) {
                    pokeApi_pokemon.setNew_id("788");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("keldeo-resolute")) {
                    pokeApi_pokemon.setNew_id("789");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("kommo-o-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("koraidon")) {
                    pokeApi_pokemon.setNew_id("1263");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("koraidon-gliding-build")) {
                    pokeApi_pokemon.setNew_id("1408");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("koraidon-limited-build")) {
                    pokeApi_pokemon.setNew_id("1405");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("koraidon-sprinting-build")) {
                    pokeApi_pokemon.setNew_id("1406");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("koraidon-swimming-build")) {
                    pokeApi_pokemon.setNew_id("1407");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("kyogre-primal")) {
                    pokeApi_pokemon.setNew_id("472");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("kyurem-black")) {
                    pokeApi_pokemon.setNew_id("787");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("kyurem-white")) {
                    pokeApi_pokemon.setNew_id("786");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("landorus-incarnate")) {
                    pokeApi_pokemon.setNew_id("783");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("landorus-therian")) {
                    pokeApi_pokemon.setNew_id("784");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("lurantis-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("lycanroc-dusk")) {
                    pokeApi_pokemon.setNew_id("958");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("lycanroc-midday")) {
                    pokeApi_pokemon.setNew_id("956");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("lycanroc-midnight")) {
                    pokeApi_pokemon.setNew_id("957");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("magearna-original")) {
                    pokeApi_pokemon.setNew_id("1295");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("marowak-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("maushold")) {
                    pokeApi_pokemon.setNew_id("1173");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("maushold-family-of-three")) {
                    pokeApi_pokemon.setNew_id("1172");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("meloetta-aria")) {
                    pokeApi_pokemon.setNew_id("790");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("meloetta-pirouette")) {
                    pokeApi_pokemon.setNew_id("791");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mewtwo-mega-x")) {
                    pokeApi_pokemon.setNew_id("202");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mewtwo-mega-y")) {
                    pokeApi_pokemon.setNew_id("203");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mime-jr")) {
                    pokeApi_pokemon.setNew_id("542");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mimikyu-busted")) {
                    pokeApi_pokemon.setNew_id("1006");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mimikyu-disguised")) {
                    pokeApi_pokemon.setNew_id("1005");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mimikyu-totem-busted")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mimikyu-totem-disguised")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-blue")) {
                    pokeApi_pokemon.setNew_id("991");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-blue-meteor")) {
                    pokeApi_pokemon.setNew_id("990");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-green")) {
                    pokeApi_pokemon.setNew_id("1001");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-green-meteor")) {
                    pokeApi_pokemon.setNew_id("1000");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-indigo")) {
                    pokeApi_pokemon.setNew_id("993");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-indigo-meteor")) {
                    pokeApi_pokemon.setNew_id("992");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-orange")) {
                    pokeApi_pokemon.setNew_id("995");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-orange-meteor")) {
                    pokeApi_pokemon.setNew_id("994");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-red")) {
                    pokeApi_pokemon.setNew_id("989");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-red-meteor")) {
                    pokeApi_pokemon.setNew_id("988");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-violet")) {
                    pokeApi_pokemon.setNew_id("997");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-violet-meteor")) {
                    pokeApi_pokemon.setNew_id("996");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-yellow")) {
                    pokeApi_pokemon.setNew_id("999");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("minior-yellow-meteor")) {
                    pokeApi_pokemon.setNew_id("998");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("miraidon")) {
                    pokeApi_pokemon.setNew_id("1268");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("miraidon-aquatic-mode")) {
                    pokeApi_pokemon.setNew_id("1411");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("miraidon-drive-mode")) {
                    pokeApi_pokemon.setNew_id("1410");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("miraidon-glide-mode")) {
                    pokeApi_pokemon.setNew_id("1412");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("miraidon-low-power-mode")) {
                    pokeApi_pokemon.setNew_id("1409");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("morpeko-full-belly")) {
                    pokeApi_pokemon.setNew_id("1113");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("morpeko-hangry")) {
                    pokeApi_pokemon.setNew_id("1114");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mr-mime")) {
                    pokeApi_pokemon.setNew_id("163");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mr-mime-galar")) {
                    pokeApi_pokemon.setNew_id("164");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("mr-rime")) {
                    pokeApi_pokemon.setNew_id("1100");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("necrozma-dawn")) {
                    pokeApi_pokemon.setNew_id("1030");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("necrozma-dusk")) {
                    pokeApi_pokemon.setNew_id("1029");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("necrozma-ultra")) {
                    pokeApi_pokemon.setNew_id("1031");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("nidoran-f")) {
                    pokeApi_pokemon.setNew_id("42");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("nidoran-m")) {
                    pokeApi_pokemon.setNew_id("45");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("ogerpon")) {
                    pokeApi_pokemon.setNew_id("1281");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("ogerpon-cornerstone-mask")) {
                    pokeApi_pokemon.setNew_id("1284");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("ogerpon-hearthflame-mask")) {
                    pokeApi_pokemon.setNew_id("1283");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("ogerpon-wellspring-mask")) {
                    pokeApi_pokemon.setNew_id("1282");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("oinkologne")) {
                    pokeApi_pokemon.setNew_id("1162");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("oricorio-baile")) {
                    pokeApi_pokemon.setNew_id("948");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("oricorio-pau")) {
                    pokeApi_pokemon.setNew_id("950");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("oricorio-pom-pom")) {
                    pokeApi_pokemon.setNew_id("949");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("oricorio-sensu")) {
                    pokeApi_pokemon.setNew_id("951");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("palafin")) {
                    pokeApi_pokemon.setNew_id("1215");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("palafin-hero")) {
                    pokeApi_pokemon.setNew_id("1216");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("palkia-origin")) {
                    pokeApi_pokemon.setNew_id("598");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-alola-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-belle")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-cosplay")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-hoenn-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-kalos-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-libre")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-original-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-partner-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-phd")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-pop-star")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-rock-star")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-sinnoh-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-starter")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-unova-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pikachu-world-cap")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pumpkaboo-average")) {
                    pokeApi_pokemon.setNew_id("905");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pumpkaboo-large")) {
                    pokeApi_pokemon.setNew_id("906");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pumpkaboo-small")) {
                    pokeApi_pokemon.setNew_id("904");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pumpkaboo-super")) {
                    pokeApi_pokemon.setNew_id("907");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("pyroar")) {
                    pokeApi_pokemon.setNew_id("836");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("raticate-totem-alola")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("ribombee-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rockruff-own-tempo")) {
                    pokeApi_pokemon.setNew_id("955");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rotom")) {
                    pokeApi_pokemon.setNew_id("586");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rotom-fan")) {
                    pokeApi_pokemon.setNew_id("590");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rotom-frost")) {
                    pokeApi_pokemon.setNew_id("589");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rotom-heat")) {
                    pokeApi_pokemon.setNew_id("587");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rotom-mow")) {
                    pokeApi_pokemon.setNew_id("591");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("rotom-wash")) {
                    pokeApi_pokemon.setNew_id("588");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("salazzle-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("sawsbuck")) {
                    pokeApi_pokemon.setNew_id("717");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("shaymin-land")) {
                    pokeApi_pokemon.setNew_id("607");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("shaymin-sky")) {
                    pokeApi_pokemon.setNew_id("608");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("shellos")) {
                    pokeApi_pokemon.setNew_id("522");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("sirfetchd")) {
                    pokeApi_pokemon.setNew_id("1099");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("squawkabilly")) {
                    pokeApi_pokemon.setNew_id("1179");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("squawkabilly-blue-plumage")) {
                    pokeApi_pokemon.setNew_id("1180");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("squawkabilly-white-plumage")) {
                    pokeApi_pokemon.setNew_id("1182");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("squawkabilly-yellow-plumage")) {
                    pokeApi_pokemon.setNew_id("1181");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tatsugiri")) {
                    pokeApi_pokemon.setNew_id("1230");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tatsugiri-droopy")) {
                    pokeApi_pokemon.setNew_id("1231");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tatsugiri-stretchy")) {
                    pokeApi_pokemon.setNew_id("1232");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tauros-paldea-aqua-breed")) {
                    pokeApi_pokemon.setNew_id("174");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tauros-paldea-blaze-breed")) {
                    pokeApi_pokemon.setNew_id("173");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tauros-paldea-combat-breed")) {
                    pokeApi_pokemon.setNew_id("172");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("terapagos-stellar")) {
                    pokeApi_pokemon.setNew_id("1293");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("terapagos-terastal")) {
                    pokeApi_pokemon.setNew_id("1292");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("thundurus-incarnate")) {
                    pokeApi_pokemon.setNew_id("779");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("thundurus-therian")) {
                    pokeApi_pokemon.setNew_id("780");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("togedemaru-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tornadus-incarnate")) {
                    pokeApi_pokemon.setNew_id("777");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("tornadus-therian")) {
                    pokeApi_pokemon.setNew_id("778");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("toxtricity-amped")) {
                    pokeApi_pokemon.setNew_id("1082");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("toxtricity-low-key")) {
                    pokeApi_pokemon.setNew_id("1083");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("unfezant")) {
                    pokeApi_pokemon.setNew_id("638");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("unown")) {
                    pokeApi_pokemon.setNew_id("258");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("ursaluna-bloodmoon")) {
                    pokeApi_pokemon.setNew_id("1339");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("urshifu-rapid-strike")) {
                    pokeApi_pokemon.setNew_id("1133");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("urshifu-single-strike")) {
                    pokeApi_pokemon.setNew_id("1132");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("vikavolt-totem")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("vivillon")) {
                    pokeApi_pokemon.setNew_id("834");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("wishiwashi-school")) {
                    pokeApi_pokemon.setNew_id("960");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("wishiwashi-solo")) {
                    pokeApi_pokemon.setNew_id("959");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("wooper-paldea")) {
                    pokeApi_pokemon.setNew_id("250");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("wormadam-plant")) {
                    pokeApi_pokemon.setNew_id("510");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("wormadam-sandy")) {
                    pokeApi_pokemon.setNew_id("511");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("wormadam-trash")) {
                    pokeApi_pokemon.setNew_id("512");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("xerneas")) {
                    pokeApi_pokemon.setNew_id("917");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zacian")) {
                    pokeApi_pokemon.setNew_id("1125");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zacian-crowned")) {
                    pokeApi_pokemon.setNew_id("1126");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zamazenta")) {
                    pokeApi_pokemon.setNew_id("1127");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zamazenta-crowned")) {
                    pokeApi_pokemon.setNew_id("1128");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zarude-dada")) {
                    pokeApi_pokemon.setNew_id("1135");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zygarde-10")) {
                    pokeApi_pokemon.setNew_id("922");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zygarde-10-power-construct")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zygarde-50")) {
                    pokeApi_pokemon.setNew_id("921");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zygarde-50-power-construct")) {
                    pokeApi_pokemon.setNew_id("DELETE THIS ENTRY");
                } else if (pokeApi_pokemon.getIdentifier().equalsIgnoreCase("zygarde-complete")) {
                    pokeApi_pokemon.setNew_id("1316");
                } else if (pokeApi_pokemon.getIdentifier().contains("-")) {
                    if (pokeApi_pokemon.getIdentifier().replace("-", " ").equalsIgnoreCase(custom_pokemon_form.getName())) {
                        pokeApi_pokemon.setNew_id(String.valueOf(custom_pokemon_form.getId()));
                    }
                }
            }
        }


        List<PokeApi_Pokemon_Move> new_list = new ArrayList<>();

        for (PokeApi_Pokemon_Move pokeApi_pokemon_move : pokeApi_pokemon_move_csv) {
            for (PokeApi_Pokemon pokeApi_pokemon : pokeApi_pokemon_csv) {
                if (pokeApi_pokemon_move.getPokemon_id().equalsIgnoreCase(String.valueOf(pokeApi_pokemon.getId()))) {
                    PokeApi_Pokemon_Move pokeApiPokemonMove_copy = new PokeApi_Pokemon_Move();
                    pokeApiPokemonMove_copy.setLevel(pokeApi_pokemon_move.getLevel());
                    pokeApiPokemonMove_copy.setOrder(pokeApi_pokemon_move.getOrder());
                    pokeApiPokemonMove_copy.setMove_id(pokeApi_pokemon_move.getMove_id());
                    pokeApiPokemonMove_copy.setPokemon_move_method_id(pokeApi_pokemon_move.getPokemon_move_method_id());
                    pokeApiPokemonMove_copy.setVersion_group_id(pokeApi_pokemon_move.getVersion_group_id());
                    pokeApiPokemonMove_copy.setPokemon_id(pokeApi_pokemon.getNew_id());
                    new_list.add(pokeApiPokemonMove_copy);
                }
            }
        }


        try (FileWriter writer = new FileWriter("forms_with_names.csv")) {
            StringBuilder builder = new StringBuilder();
            builder.append("form_id;nat_dex_id;name;form_name");
            for (Custom_Pokemon_Form custom_pokemon_form : custom_pokemon_form_csv) {
                builder.append(custom_pokemon_form.getId())
                       .append(";")
                       .append(custom_pokemon_form.getNat_dex_id())
                       .append(";")
                       .append(custom_pokemon_form.getName())
                       .append(";")
                       .append(custom_pokemon_form.getForm_name())
                       .append("\n");
            }
            writer.write(builder.toString());
        }

        writeToCsv(new_list, "pokemon_id;version_group_id;move_id;pokemon_move_method_id;level;order");
    }

    @SneakyThrows
    public static void writeToCsv(List<PokeApi_Pokemon_Move> sampleList, String header) {
        PrintWriter writer = new PrintWriter("pokemon_move.csv");
        writer.println(header);

        for (PokeApi_Pokemon_Move sample : sampleList) {
            writer.println(sample);
        }
        writer.close();
    }
}
