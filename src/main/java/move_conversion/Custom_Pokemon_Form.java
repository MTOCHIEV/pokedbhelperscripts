package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"id", "nat_dex_id", "form_name", "type_1", "type_2", "pokemon_category", "shape_id", "footprint_image_path", "pokedex_color", "height_m", "weight_kg", "ability_1", "ability_2", "hidden_ability", "ev_yield_hp", "ev_yield_atk", "ev_yield_def", "ev_yield_spatk", "ev_yield_spdef", "ev_yield_speed", "main_image_normal", "main_image_shiny", "pokemon_cry_path", "stat_hp", "stat_attack", "stat_defense", "stat_spatk", "stat_spdef", "stat_speed", "stat_total", "base_experience", "default_form", "include"})
public class Custom_Pokemon_Form {

    private String ability_1;
    private String ability_2;
    private String base_experience;
    private boolean default_form;
    private int ev_yield_atk;
    private int ev_yield_def;
    private int ev_yield_hp;
    private int ev_yield_spatk;
    private int ev_yield_spdef;
    private int ev_yield_speed;
    private String footprint_image_path;
    private String form_name;
    private String height_m;
    private String hidden_ability;
    private int id;
    private boolean include;
    private String main_image_normal;
    private String main_image_shiny;
    private String name;
    private int nat_dex_id;
    private String pokedex_color;
    private String pokemon_category;
    private String pokemon_cry_path;
    private int shape_id;
    private int stat_attack;
    private int stat_defense;
    private int stat_hp;
    private int stat_spatk;
    private int stat_spdef;
    private int stat_speed;
    private int stat_total;
    private int type_1;
    private String type_2;
    private String weight_kg;

    public void setIs_default_form(String default_form) {
        this.default_form = default_form.equalsIgnoreCase("true");
    }

    public void setInclude(String include) {
        this.include = include.equalsIgnoreCase("true");
    }


    @Override
    public String toString() {
        StringBuilder dataBuilder = new StringBuilder();
        appendFieldValue(dataBuilder, String.valueOf(id));
        appendFieldValue(dataBuilder, String.valueOf(type_1));
        appendFieldValue(dataBuilder, String.valueOf(type_2));
        return dataBuilder.toString();
    }

    private void appendFieldValue(StringBuilder dataBuilder, String fieldValue) {
        if (fieldValue != null) {
            dataBuilder.append(fieldValue).append(";");
        } else {
            dataBuilder.append(";");
        }
    }
}

