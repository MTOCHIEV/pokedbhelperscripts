package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"ndex_id", "name_english", "name_german", "name_french", "name_spanish", "name_italian", "name_japanese", "name_japanese_romanized", "name_korean", "name_korean_romanized", "name_chinese_traditional", "name_chinese_simplified", "name_chinese_romanized"})
public class Custom_Pokemon_Name {

    private String name_chinese_romanized;
    private String name_chinese_simplified;
    private String name_chinese_traditional;
    private String name_english;
    private String name_french;
    private String name_german;
    private String name_italian;
    private String name_japanese;
    private String name_japanese_romanized;
    private String name_korean;
    private String name_korean_romanized;
    private String name_spanish;
    private int ndex_id;

}

