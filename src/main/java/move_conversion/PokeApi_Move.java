package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"id", "identifier", "generation_id", "type_id", "power", "pp", "accuracy", "priority", "target_id", "damage_class_id", "effect_id", "effect_chance", "contest_type_id", "contest_effect_id", "super_contest_effect_id"})
public class PokeApi_Move {

    private int accuracy;
    private int contest_effect_id;
    private int contest_type_id;
    private int damage_class_id;
    private int effect_chance;
    private int effect_id;
    private int generation_id;
    private int id;
    private String identifier;
    private int power;
    private int pp;
    private int priority;
    private int super_contest_effect_id;
    private int target_id;
    private int type_id;
}

