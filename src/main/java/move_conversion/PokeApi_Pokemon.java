package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"id", "identifier", "species_id", "height", "weight", "base_experience", "order", "is_default"})
public class PokeApi_Pokemon {

    private int base_experience;
    private int height;
    private String id;
    private String identifier;
    private boolean is_default;
    private int order;
    private int species_id;
    private int weight;
    private String new_id;

    public void setIs_default(String is_default) {
        this.is_default = is_default.equalsIgnoreCase("1");
    }
}

