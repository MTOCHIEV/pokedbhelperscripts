package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"pokemon_id", "version_group_id", "move_id", "pokemon_move_method_id", "level", "order"})
public class PokeApi_Pokemon_Move {

    private int level;
    private int move_id;
    private int order;
    private String pokemon_id;
    private int pokemon_move_method_id;
    private int version_group_id;


    @Override
    public String toString() {
        StringBuilder dataBuilder = new StringBuilder();
        appendFieldValue(dataBuilder, pokemon_id);
        appendFieldValue(dataBuilder, String.valueOf(version_group_id));
        appendFieldValue(dataBuilder, String.valueOf(move_id));
        appendFieldValue(dataBuilder, String.valueOf(pokemon_move_method_id));
        appendFieldValue(dataBuilder, String.valueOf(level));
        appendFieldValue(dataBuilder, String.valueOf(order));
        return dataBuilder.toString();
    }

    private void appendFieldValue(StringBuilder dataBuilder, String fieldValue) {
        if (fieldValue != null) {
            dataBuilder.append(fieldValue).append(";");
        } else {
            dataBuilder.append(";");
        }
    }
}
