package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"id", "identifier"})
public class PokeApi_Pokemon_Move_Method {

    private int id;
    private String identifier;
}
