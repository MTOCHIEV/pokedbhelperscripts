package pokedbutils.move_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"pokemon_move_method_id", "language", "name", "description"})
public class PokeApi_Pokemon_Move_Method_Prose {

    private String description;
    private String language;
    private String name;
    private int pokemon_move_method_id;
}
