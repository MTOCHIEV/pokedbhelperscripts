package pokedbutils.move_conversion;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class Utils {
    public static final String format = "%1$-50s%2$-50s\n";

    @SneakyThrows
    public static <T> void print_list_as_json(List<T> list) {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(list);
        System.out.println(json);
    }

    @SneakyThrows
    public static <T> List<T> convert_csv_to_entry_list(Class<T> t, String csv_path) {
        URL csv_url = Utils.class.getClassLoader().getResource(csv_path);
        CsvSchema schema = CsvSchema.emptySchema().withHeader().withColumnSeparator(';');
        MappingIterator<T> iterator = new CsvMapper().readerWithSchemaFor(t).with(schema).readValues(csv_url);
        return iterator.readAll();
    }
}
