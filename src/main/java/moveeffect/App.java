package pokedbutils.moveeffect;

import pokedbutils.move_conversion.Utils;

import java.util.List;

public class App {
    public static void main(String[] args) {
        List<CSV_Move_Effect> csv_ability_proses = Utils.convert_csv_to_entry_list(CSV_Move_Effect.class, "move_effect.csv");

        csv_ability_proses.forEach(entry -> {
            String[] splitparts = entry.getEffect().split("((?=\\[)|(?<=\\}))");
            int id = 0;
            for (String splitpart : splitparts) {
                id++;
                String text;
                String icon;
                String is_link;
                String link_target;
                String hex_color;
                if (splitpart.equalsIgnoreCase("[accuracy]{mechanic:accuracy}")) {
                    text = "accuracy";
                    icon = "pokemon_data/assets/effect_icons/stat_accuracy.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Attack]{mechanic:attack}")) {
                    text = "Attack";
                    icon = "pokemon_data/assets/effect_icons/stat_attack.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burned]{mechanic:burn}")) {
                    text = "burned";
                    icon = "pokemon_data/assets/effect_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burning]{mechanic:burn}")) {
                    text = "burning";
                    icon = "pokemon_data/assets/effect_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burns]{mechanic:burn}")) {
                    text = "burns";
                    icon = "pokemon_data/assets/effect_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[burn]{mechanic:burn}")) {
                    text = "burn";
                    icon = "pokemon_data/assets/effect_icons/status_burn.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[freeze]{mechanic:freeze}")) {
                    text = "freeze";
                    icon = "pokemon_data/assets/effect_icons/status_frozen.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[confused]{mechanic:confusion}")) {
                    text = "confused";
                    icon = "pokemon_data/assets/effect_icons/status_confusion.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[confused]{mechanic:confused}")) {
                    text = "confused";
                    icon = "pokemon_data/assets/effect_icons/status_confusion.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Switch]{mechanic:switch}")) {
                    text = "Switch";
                    icon = "pokemon_data/assets/effect_icons/swap.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                }  else if (splitpart.equalsIgnoreCase("[faint]{mechanic:faint}")) {
                    text = "faint";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[fail]{mechanic:fail}")) {
                    text = "fail";
                    icon = "pokemon_data/assets/effect_icons/fail.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Fails]{mechanic:fail}")) {
                    text = "Fails";
                    icon = "pokemon_data/assets/effect_icons/fail.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[miss]{mechanic:miss}")) {
                    text = "miss";
                    icon = "pokemon_data/assets/effect_icons/miss.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[confusion]{mechanic:confusion}")) {
                    text = "confusion";
                    icon = "pokemon_data/assets/effect_icons/status_confusion.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[critical hits]{mechanic:critical-hit}")) {
                    text = "critical hits";
                    icon = "pokemon_data/assets/effect_icons/critical_hit.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[critical hit]{mechanic:critical-hit}")) {
                    text = "critical hit";
                    icon = "pokemon_data/assets/effect_icons/critical_hit.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Defense]{mechanic:defense}")) {
                    text = "Defense";
                    icon = "pokemon_data/assets/effect_icons/stat_defense.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Disabling]{move:disable}")) {
                    text = "Disabling";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move?move=disable";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[evasion]{mechanic:evasion}")) {
                    text = "evasion";
                    icon = "pokemon_data/assets/effect_icons/stat_evasion_up.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[flinching]{mechanic:flinching}")) {
                    text = "flinching";
                    icon = "pokemon_data/assets/effect_icons/status_flinch.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[flinch]{mechanic:flinch}")) {
                    text = "flinch";
                    icon = "pokemon_data/assets/effect_icons/status_flinch.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Flung]{move:fling}")) {
                    text = "Flung";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=fling";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[freezing]{mechanic:freezing}")) {
                    text = "freezing";
                    icon = "pokemon_data/assets/effect_icons/status_frozen.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[hail]{mechanic:hail}")) {
                    text = "hail";
                    icon = "pokemon_data/assets/effect_icons/weather_blizzard.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[HP]{mechanic:hp}")) {
                    text = "HP";
                    icon = "pokemon_data/assets/effect_icons/stat_hp.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[infatuating]{mechanic:infatuation}")) {
                    text = "infatuating";
                    icon = "pokemon_data/assets/effect_icons/status_infatuated.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "";
                } else if (splitpart.equalsIgnoreCase("[infatuation]{mechanic:infatuation}")) {
                    text = "infatuation";
                    icon = "pokemon_data/assets/effect_icons/status_infatuated.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[lowered]{mechanic:stat-modifiers}")) {
                    text = "lowered";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[lowered]{mechanic:stat-modifier}")) {
                    text = "lowered";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[major status ailments]{mechanic:major-status-ailments}")) {
                    text = "major status ailments";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[major status ailment]{mechanic:major-status-ailment}")) {
                    text = "major status ailment";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[not-very-effective]{mechanic:not-very-effective}")) {
                    text = "not-very-effective";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[paralysis]{mechanic:paralysis}")) {
                    text = "paralysis";
                    icon = "pokemon_data/assets/effect_icons/status_paralysis.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[paralyzing]{mechanic:paralysis}")) {
                    text = "paralyzing";
                    icon = "pokemon_data/assets/effect_icons/status_paralysis.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[poisoned]{mechanic:poison}")) {
                    text = "poisoned";
                    icon = "pokemon_data/assets/effect_icons/status_poison.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[poisoning]{mechanic:poison}")) {
                    text = "poisoning";
                    icon = "pokemon_data/assets/effect_icons/status_poison.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[poison]{mechanic:poison}")) {
                    text = "poison";
                    icon = "pokemon_data/assets/effect_icons/status_poison.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[PP]{mechanic:pp}")) {
                    text = "PP";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[rain]{mechanic:rain}")) {
                    text = "rain";
                    icon = "pokemon_data/assets/effect_icons/weather_rain.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[same-type attack bonus]{mechanic:same-type-attack-bonus}")) {
                    text = "same-type attack bonus";
                    icon = "pokemon_data/assets/effect_icons/stab_bonus.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[sandstorm]{mechanic:sandstorm}")) {
                    text = "sandstorm";
                    icon = "pokemon_data/assets/effect_icons/weather_sandstorm.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[sleeping]{mechanic:sleep}")) {
                    text = "sleeping";
                    icon = "pokemon_data/assets/effect_icons/status_sleep.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[sleep]{mechanic:sleep}")) {
                    text = "sleep";
                    icon = "pokemon_data/assets/effect_icons/status_sleep.png";
                    is_link = "TRUE";
                    link_target = "/status-conditions.xhtml";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Special Attack]{mechanic:special-attack}")) {
                    text = "Special Attack";
                    icon = "pokemon_data/assets/effect_icons/stat_spatk.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Special Defense]{mechanic:special-defense}")) {
                    text = "Special Defense";
                    icon = "pokemon_data/assets/effect_icons/stat_spdef.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Speed]{mechanic:speed}")) {
                    text = "Speed";
                    icon = "pokemon_data/assets/effect_icons/stat_speed.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stages]{mechanic:stat-modifier}")) {
                    text = "stages";
                    icon = "-";
                    is_link = "-";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stage]{mechanic:stat-modifier}")) {
                    text = "stage";
                    icon = "-";
                    is_link = "-";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stat changes]{mechanic:stat-modifiers}")) {
                    text = "stat changes";
                    icon = "pokemon_data/assets/effect_icons/stat_stats.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stat modifiers]{mechanic:stat-modifiers}")) {
                    text = "stat modifiers";
                    icon = "pokemon_data/assets/effect_icons/stat_stats.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[strong sunlight]{mechanic:strong-sunlight}")) {
                    text = "strong sunlight";
                    icon = "pokemon_data/assets/effect_icons/weather_harsh_sunlight.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[super effective]{mechanic:super-effective}")) {
                    text = "super-effective";
                    icon = "pokemon_data/assets/effect_icons/super-effective.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[super-effective]{mechanic:super-effective}")) {
                    text = "super-effective";
                    icon = "pokemon_data/assets/effect_icons/super-effective.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[Transforms]{move:transform}")) {
                    text = "Transforms";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=transform";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[weather]{mechanic:weather}")) {
                    text = "weather";
                    icon = "pokemon_data/assets/effect_icons/weather_weather.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:aftermath}")) {
                    text = "Aftermath";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=aftermath";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:dark-aura}")) {
                    text = "Dark Aura";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=dark-aura";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:fairy-aura}")) {
                    text = "Fairy Aura";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=fairy-aura";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:minus}")) {
                    text = "Minus";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=minus";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{ability:plus}")) {
                    text = "Plus";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/ability.xhtml?ability=plus";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{item:honey}")) {
                    text = "Honey";
                    icon = "pokemon_data/assets/effect_icons/honey.png";
                    is_link = "TRUE";
                    link_target = "/item.xhtml?item=honey";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:captivate}")) {
                    text = "Captivate";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=captivate";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:explosion}")) {
                    text = "Explosion";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=explosion";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:grassy-terrain}")) {
                    text = "Grassy Terrain";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=grassy-terrain";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:kings-shield}")) {
                    text = "King's Shield";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=kings-shield";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:light-screen}")) {
                    text = "Light Screen";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=light-screen";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:reflect}")) {
                    text = "Reflect";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=reflect";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:safeguard}")) {
                    text = "Safeguard";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=safeguard";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:self-destruct}")) {
                    text = "Self Destruct";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=self-destruct";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:triple-kick}")) {
                    text = "Triple Kick";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=triple-kick";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:aegislash}")) {
                    text = "Aegislash";
                    icon = "pokemon_data/assets/effect_icons/aegislash.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=aegislash";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:arceus}")) {
                    text = "Arceus";
                    icon = "pokemon_data/assets/effect_icons/arceus.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=arceus";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:castform}")) {
                    text = "Castform";
                    icon = "pokemon_data/assets/effect_icons/castform.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=castform";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{pokemon:darmanitan}")) {
                    text = "Darmanitan";
                    icon = "pokemon_data/assets/effect_icons/darmanitan.png";
                    is_link = "TRUE";
                    link_target = "/pokemon.xhtml?pokemon=darmanitan";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{type:bug}")) {
                    text = "BUG";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_bug_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=bug";
                    hex_color = "#95A235";
                } else if (splitpart.equalsIgnoreCase("[]{type:dark}")) {
                    text = "DARK";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_dark_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=dark";
                    hex_color = "#705848";
                } else if (splitpart.equalsIgnoreCase("[]{type:electric}")) {
                    text = "ELECTRIC";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_electric_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=electric";
                    hex_color = "#F2C341";
                } else if (splitpart.equalsIgnoreCase("[]{type:fairy}")) {
                    text = "FAIRY";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_fairy_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=fairy";
                    hex_color = "#74A3BA";
                } else if (splitpart.equalsIgnoreCase("[]{type:fighting}")) {
                    text = "FIGHTING";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_fighting_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=fighting";
                    hex_color = "#F08832";
                } else if (splitpart.equalsIgnoreCase("[]{type:fire}")) {
                    text = "FIRE";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_fire_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=fire";
                    hex_color = "#D33A30";
                } else if (splitpart.equalsIgnoreCase("[]{type:flying}")) {
                    text = "FLYING";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_flying_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=flying";
                    hex_color = "#8FB8E5";
                } else if (splitpart.equalsIgnoreCase("[]{type:ghost}")) {
                    text = "GHOST";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_ghost_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=ghost";
                    hex_color = "#6C436E";
                } else if (splitpart.equalsIgnoreCase("[]{type:grass}")) {
                    text = "GRASS";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_grass_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=grass";
                    hex_color = "#5D9D3C";
                } else if (splitpart.equalsIgnoreCase("[]{type:ground}")) {
                    text = "GROUND";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_ground_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=ground";
                    hex_color = "#89532A";
                } else if (splitpart.equalsIgnoreCase("[]{type:ice}")) {
                    text = "ICE";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_ice_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=ice";
                    hex_color = "#79CCEF";
                } else if (splitpart.equalsIgnoreCase("[]{type:normal}")) {
                    text = "NORMAL";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_normal_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=normal";
                    hex_color = "#A1A1A1";
                } else if (splitpart.equalsIgnoreCase("[]{type:rock}")) {
                    text = "ROCK";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_rock_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=rock";
                    hex_color = "#ADAA84";
                } else if (splitpart.equalsIgnoreCase("[]{type:steel}")) {
                    text = "STEEL";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_steel_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=steel";
                    hex_color = "#4D3F3F";
                } else if (splitpart.equalsIgnoreCase("[]{type:water}")) {
                    text = "WATER";
                    icon = "pokemon_data/assets/pokemon_type_icons/cmn_type_water_icon.png";
                    is_link = "TRUE";
                    link_target = "/type.xhtml?type=water";
                    hex_color = "#4D79BC";
                } else if (splitpart.equalsIgnoreCase("[Drains]{mechanic:drain}")) {
                    text = "Drains";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[field]{mechanic:field}")) {
                    text = "field";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[no effect]{mechanic:no-effect}")) {
                    text = "no effect";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[block]{mechanic:block}")) {
                    text = "block";
                    icon = "pokemon_data/assets/effect_icons/crit_shield.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:acid-armor}")) {
                    text = "Acid Armor";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=acid-armor";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:acupressure}")) {
                    text = "Acupressure";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=acupressure";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:after-you}")) {
                    text = "After You";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=after-you";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:agility}")) {
                    text = "Agility";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=agility";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:ally-switch}")) {
                    text = "Ally Switch";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=ally-switch";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:amnesia}")) {
                    text = "Amnesia";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=amnesia";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:aqua-ring}")) {
                    text = "Aqua Ring";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=aqua-ring";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:aromatherapy}")) {
                    text = "Aromatherapy";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=aromatherapy";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:aromatic-mist}")) {
                    text = "Aromatic Mist";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=aromatic-mist";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:assist}")) {
                    text = "Assist";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=assist";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:autotomize}")) {
                    text = "Autotomize";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=autotomize";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:barrier}")) {
                    text = "Barrier";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=barrier";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:baton-pass}")) {
                    text = "Baton Pass";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=baton-pass";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:belch}")) {
                    text = "Belch";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=belch";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:belly-drum}")) {
                    text = "Belly Drum";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=belly-drum";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:bide}")) {
                    text = "Bide";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=bide";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:bulk-up}")) {
                    text = "Bulk Up";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=bulk-up";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:calm-mind}")) {
                    text = "Calm Mind";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=calm-mind";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:camouflage}")) {
                    text = "Camouflage";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=camouflage";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:celebrate}")) {
                    text = "Celebrate";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=celebrate";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:charge}")) {
                    text = "Charge";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=charge";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:coil}")) {
                    text = "Coil";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=coil";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:conversion}")) {
                    text = "Conversion";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=conversion";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:conversion-2}")) {
                    text = "Conversion 2";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=conversion-2";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:copycat}")) {
                    text = "Copycat";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=copycat";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:cosmic-power}")) {
                    text = "Cosmic Power";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=cosmic-power";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:cotton-guard}")) {
                    text = "Cotton Guard";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=cotton-guard";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:counter}")) {
                    text = "Counter";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=counter";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:crafty-shield}")) {
                    text = "Crafty Shield";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=crafty-shield";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:curse}")) {
                    text = "Curse";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=curse";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:defend-order}")) {
                    text = "Defend Order";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=defend-order";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:defense-curl}")) {
                    text = "Defense Curl";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=defense-curl";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:destiny-bond}")) {
                    text = "Destiny Bond";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=destiny-bond";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:detect}")) {
                    text = "Detect";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=detect";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:doom-desire}")) {
                    text = "Doom Desire";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=doom-desire";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:double-team}")) {
                    text = "Double Team";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=double-team";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:dragon-dance}")) {
                    text = "Dragon Dance";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=dragon-dance";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:electric-terrain}")) {
                    text = "Electric Terrain";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=electric-terrain";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:endure}")) {
                    text = "Endure";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=endure";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:final-gambit}")) {
                    text = "Final Gambit";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=final-gambit";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:flower-shield}")) {
                    text = "Flower Shield";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=flower-shield";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:focus-energy}")) {
                    text = "Focus Energy";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=focus-energy";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:focus-punch}")) {
                    text = "Focus Punch";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=focus-punch";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:follow-me}")) {
                    text = "Follow Me";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=follow-me";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:future-sight}")) {
                    text = "Future Sight";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=future-sight";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:geomancy}")) {
                    text = "Geomancy";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=geomancy";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:gravity}")) {
                    text = "Gravity";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=gravity";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:growth}")) {
                    text = "Growth";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=growth";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:grudge}")) {
                    text = "Grudge";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=grudge";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:guard-split}")) {
                    text = "Guard Split";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=guard-split";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:hail}")) {
                    text = "Hail";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=hail";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:happy-hour}")) {
                    text = "Happy Hour";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=happy-hour";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:harden}")) {
                    text = "Harden";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=harden";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:haze}")) {
                    text = "Haze";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=haze";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:heal-bell}")) {
                    text = "Heal Bell";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=heal-bell";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:heal-order}")) {
                    text = "Heal Order";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=heal-order";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:heal-pulse}")) {
                    text = "Heal Pulse";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=heal-pulse";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:healing-wish}")) {
                    text = "Healing Wish";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=healing-wish";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:helping-hand}")) {
                    text = "Helping Hand";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=helping-hand";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:hold-hands}")) {
                    text = "Hold Hands";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=hold-hands";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:hone-claws}")) {
                    text = "Hone Claws";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=hone-claws";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:howl}")) {
                    text = "Howl";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=howl";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:imprison}")) {
                    text = "Imprison";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=imprison";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:ingrain}")) {
                    text = "Ingrain";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=ingrain";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:ion-deluge}")) {
                    text = "Ion Deluge";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=ion-deluge";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:iron-defense}")) {
                    text = "Iron Defense";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=iron-defense";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:lucky-chant}")) {
                    text = "Lucky Chant";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=lucky-chant";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:lunar-dance}")) {
                    text = "Lunar Dance";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=lunar-dance";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:magic-coat}")) {
                    text = "Magic Coat";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=magic-coat";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:magnet-rise}")) {
                    text = "Magnet Rise";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=magnet-rise";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:magnetic-flux}")) {
                    text = "Magnetic Flux";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=magnetic-flux";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:mat-block}")) {
                    text = "Mat Block";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=mat-block";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:me-first}")) {
                    text = "Me First";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=me-first";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:meditate}")) {
                    text = "Meditate";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=meditate";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:metronome}")) {
                    text = "Metronome";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=metronome";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:milk-drink}")) {
                    text = "Milk Drink";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=milk-drink";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:mimic}")) {
                    text = "Mimic";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=mimic";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:minimize}")) {
                    text = "Minimize";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=minimize";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:mirror-coat}")) {
                    text = "Mirror Coat";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=mirror-coat";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:mirror-move}")) {
                    text = "Mirror Move";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=mirror-move";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:mist}")) {
                    text = "Mist";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=mist";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:misty-terrain}")) {
                    text = "Misty Terrain";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=misty-terrain";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:moonlight}")) {
                    text = "Moonlight";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=moonlight";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:morning-sun}")) {
                    text = "Morning Sun";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=morning-sun";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:mud-sport}")) {
                    text = "Mud Sport";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=mud-sport";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:nasty-plot}")) {
                    text = "Nasty Plot";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=nasty-plot";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:nature-power}")) {
                    text = "Nature Power";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=nature-power";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:perish-song}")) {
                    text = "Perish Song";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=perish-song";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:power-split}")) {
                    text = "Power Split";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=power-split";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:power-trick}")) {
                    text = "Power Trick";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=power-trick";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:protect}")) {
                    text = "Protect";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=protect";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:psych-up}")) {
                    text = "Psych Up";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=psych-up";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:quick-guard}")) {
                    text = "Quick Guard";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=quick-guard";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:quiver-dance}")) {
                    text = "Quiver Dance";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=quiver-dance";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:rage-powder}")) {
                    text = "Rage Powder";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=rage-powder";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:rain-dance}")) {
                    text = "Rain Dance";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=rain-dance";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:recover}")) {
                    text = "Recover";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=recover";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:recycle}")) {
                    text = "Recycle";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=recycle";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:reflect-type}")) {
                    text = "Reflect Type";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=reflect-type";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:refresh}")) {
                    text = "Refresh";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=refresh";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:rest}")) {
                    text = "Rest";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=rest";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:rock-polish}")) {
                    text = "Rock Polish";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=rock-polish";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:role-play}")) {
                    text = "Role Play";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=role-play";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:roost}")) {
                    text = "Roost";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=roost";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:rototiller}")) {
                    text = "Rototiller";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=rototiller";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sandstorm}")) {
                    text = "Sandstorm";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sandstorm";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-blast}")) {
                    text = "Shadow Blast";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-blast";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-bolt}")) {
                    text = "Shadow Bolt";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-bolt";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-half}")) {
                    text = "Shadow Half";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-half";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-rush}")) {
                    text = "Shadow Rush";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-rush";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-shed}")) {
                    text = "Shadow Shed";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-shed";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-sky}")) {
                    text = "Shadow Sky";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-sky";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-storm}")) {
                    text = "Shadow Storm";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-storm";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shadow-wave}")) {
                    text = "Shadow Wave";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shadow-wave";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sharpen}")) {
                    text = "Sharpen";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sharpen";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shell-smash}")) {
                    text = "Shell Smash";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shell-smash";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:shift-gear}")) {
                    text = "Shift Gear";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=shift-gear";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sketch}")) {
                    text = "Sketch";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sketch";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:slack-off}")) {
                    text = "Slack Off";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=slack-off";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sleep-talk}")) {
                    text = "Sleep Talk";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sleep-talk";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:snatch}")) {
                    text = "Snatch";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=snatch";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:soft-boiled}")) {
                    text = "Soft Boiled";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=soft-boiled";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:spikes}")) {
                    text = "Spikes";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=spikes";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:spiky-shield}")) {
                    text = "Spiky Shield";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=spiky-shield";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:spit-up}")) {
                    text = "Spit Up";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=spit-up";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:splash}")) {
                    text = "Splash";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=splash";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:stealth-rock}")) {
                    text = "Stealth Rock";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=stealth-rock";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sticky-web}")) {
                    text = "Sticky Web";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sticky-web";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:stockpile}")) {
                    text = "Stockpile";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=stockpile";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:struggle}")) {
                    text = "Struggle";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=struggle";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:substitute}")) {
                    text = "Substitute";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=substitute";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sunny-day}")) {
                    text = "Sunny Day";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sunny-day";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:swallow}")) {
                    text = "Swallow";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=swallow";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:swords-dance}")) {
                    text = "Swords Dance";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=swords-dance";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:synthesis}")) {
                    text = "Synthesis";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=synthesis";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:tail-glow}")) {
                    text = "Tail Glow";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=tail-glow";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:tailwind}")) {
                    text = "Tailwind";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=tailwind";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:teleport}")) {
                    text = "Teleport";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=teleport";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:toxic-spikes}")) {
                    text = "Toxic Spikes";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=toxic-spikes";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:transform}")) {
                    text = "Transform";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=transform";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:water-sport}")) {
                    text = "Water Sport";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=water-sport";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:wide-guard}")) {
                    text = "Wide Guard";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=wide-guard";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:wish}")) {
                    text = "Wish";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=wish";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:withdraw}")) {
                    text = "Withdraw";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=withdraw";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:work-up}")) {
                    text = "Work Up";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=work-up";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:assist}")) {
                    text = "Assist";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=assist";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:metronome}")) {
                    text = "Metronome";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=metronome";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:sleep-talk}")) {
                    text = "Sleep Talk";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=sleep-talk";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:encore}")) {
                    text = "Encore";
                    icon = "-";
                    is_link = "TRUE";
                    link_target = "/move.xhtml?move=encore";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[stage]{mechanic:stage}")) {
                    text = "stage";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                }else if (splitpart.equalsIgnoreCase("[stages]{mechanic:stage}")) {
                    text = "stages";
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                }  else if (splitpart.equalsIgnoreCase("[stat]{mechanic:stat}")) {
                    text = "stat";
                    icon = "pokemon_data/assets/effect_icons/stat_stats.png";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                } else if (splitpart.equalsIgnoreCase("[]{move:destiny-bond}")){
                    text="[]{move:destiny Bond";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=[]{move:destiny-bond";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:air-lock}")){
                    text="Air Lock";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=air-lock";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:arena-trap}")){
                    text="Arena Trap";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=arena-trap";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:chlorophyll}")){
                    text="Chlorophyll";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=chlorophyll";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:clear-body}")){
                    text="Clear Body";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=clear-body";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:cloud-nine}")){
                    text="Cloud Nine";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=cloud-nine";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:disguise}")){
                    text="Disguise";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=disguise";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:dry-skin}")){
                    text="Dry Skin";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=dry-skin";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:early-bird}")){
                    text="Early Bird";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=early-bird";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:flower-gift}")){
                    text="Flower Gift";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=flower-gift";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:forecast}")){
                    text="Forecast";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=forecast";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:hydration}")){
                    text="Hydration";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=hydration";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:illusion}")){
                    text="Illusion";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=illusion";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:imposter}")){
                    text="Imposter";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=imposter";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:insomnia}")){
                    text="Insomnia";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=insomnia";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:intimidate}")){
                    text="Intimidate";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=intimidate";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:iron-fist}")){
                    text="Iron Fist";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=iron-fist";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:leaf-guard}")){
                    text="Leaf Guard";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=leaf-guard";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:levitate}")){
                    text="Levitate";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=levitate";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:lightning-rod}")){
                    text="Lightning Rod";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=lightning-rod";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:liquid-ooze}")){
                    text="Liquid Ooze";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=liquid-ooze";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:magic-guard}")){
                    text="Magic Guard";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=magic-guard";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:multitype}")){
                    text="Multitype";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=multitype";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:no-guard}")){
                    text="No Guard";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=no-guard";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:pressure}")){
                    text="Pressure";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=pressure";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:rain-dish}")){
                    text="Rain Dish";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=rain-dish";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:rock-head}")){
                    text="Rock Head";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=rock-head";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:simple}")){
                    text="Simple";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=simple";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:skill-link}")){
                    text="Skill Link";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=skill-link";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:snow-cloak}")){
                    text="Snow Cloak";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=snow-cloak";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:solar-power}")){
                    text="Solar Power";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=solar-power";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:stall}")){
                    text="Stall";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=stall";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:stance-change}")){
                    text="Stance Change";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=stance-change";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:sticky-hold}")){
                    text="Sticky Hold";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=sticky-hold";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:storm-drain}")){
                    text="Storm Drain";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=storm-drain";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:suction-cups}")){
                    text="Suction Cups";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=suction-cups";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:swift-swim}")){
                    text="Swift Swim";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=swift-swim";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:synchronize}")){
                    text="Synchronize";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=synchronize";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:trace}")){
                    text="Trace";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=trace";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:truant}")){
                    text="Truant";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=truant";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:vital-spirit}")){
                    text="Vital Spirit";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=vital-spirit";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:volt-absorb}")){
                    text="Volt Absorb";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=volt-absorb";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:white-smoke}")){
                    text="White Smoke";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=white-smoke";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:wonder-guard}")){
                    text="Wonder Guard";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=wonder-guard";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{ability:zen-mode}")){
                    text="Zen Mode";
                    icon="-";
                    is_link="TRUE";
                    link_target="/ability.xhtml?ability=zen-mode";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:destiny-bond}")){
                    text="[]{move:destiny Bond";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=[]{move:destiny-bond";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:choice-band}")){
                    text="Choice Band";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=choice-band";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:choice-scarf}")){
                    text="Choice Scarf";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=choice-scarf";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:choice-specs}")){
                    text="Choice Specs";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=choice-specs";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:damp-rock}")){
                    text="Damp Rock";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=damp-rock";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:focus-sash}")){
                    text="Focus Sash";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=focus-sash";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:full-incense}")){
                    text="Full Incense";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=full-incense";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:heat-rock}")){
                    text="Heat Rock";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=heat-rock";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:icy-rock}")){
                    text="Icy Rock";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=icy-rock";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:jaboca-berry}")){
                    text="Jaboca Berry";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=jaboca-berry";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:lagging-tail}")){
                    text="Lagging Tail";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=lagging-tail";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:lansat-berry}")){
                    text="Lansat Berry";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=lansat-berry";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:life-orb}")){
                    text="Life Orb";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=life-orb";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:light-clay}")){
                    text="Light Clay";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=light-clay";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:quick-claw}")){
                    text="Quick Claw";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=quick-claw";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:rowap-berry}")){
                    text="Rowap Berry";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=rowap-berry";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{item:shed-shell}")){
                    text="Shed Shell";
                    icon="-";
                    is_link="TRUE";
                    link_target="/item.xhtml?item=shed-shell";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:destiny-bond}")){
                    text="Destiny Bond";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=destiny-bond";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{mechanic:flinch}")){
                    text="[]{mechanic:flinch";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=[]{mechanic:flinch";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{mechanic:stage}")){
                    text="[]{mechanic:stage";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=[]{mechanic:stage";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:absorb}")){
                    text="Absorb";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=absorb";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:attract}")){
                    text="Attract";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=attract";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:avalanche}")){
                    text="Avalanche";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=avalanche";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:bind}")){
                    text="Bind";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=bind";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:blast-burn}")){
                    text="Blast Burn";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=blast-burn";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:blizzard}")){
                    text="Blizzard";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=blizzard";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:block}")){
                    text="Block";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=block";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:bounce}")){
                    text="Bounce";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=bounce";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:brick-break}")){
                    text="Brick Break";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=brick-break";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:bug-bite}")){
                    text="Bug Bite";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=bug-bite";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:chatter}")){
                    text="Chatter";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=chatter";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:circle-throw}")){
                    text="Circle Throw";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=circle-throw";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:clamp}")){
                    text="Clamp";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=clamp";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:covet}")){
                    text="Covet";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=covet";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:defog}")){
                    text="Defog";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=defog";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:dig}")){
                    text="Dig";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=dig";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:disable}")){
                    text="Disable";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=disable";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:dive}")){
                    text="Dive";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=dive";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:dragon-tail}")){
                    text="Dragon Tail";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=dragon-tail";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:dream-eater}")){
                    text="Dream Eater";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=dream-eater";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:earthquake}")){
                    text="Earthquake";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=earthquake";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:embargo}")){
                    text="Embargo";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=embargo";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:endeavor}")){
                    text="Endeavor";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=endeavor";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:energy-ball}")){
                    text="Energy Ball";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=energy-ball";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:feint}")){
                    text="Feint";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=feint";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fire-pledge}")){
                    text="Fire Pledge";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fire-pledge";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fire-spin}")){
                    text="Fire Spin";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fire-spin";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fissure}")){
                    text="Fissure";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fissure";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:flatter}")){
                    text="Flatter";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=flatter";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fling}")){
                    text="Fling";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fling";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fly}")){
                    text="Fly";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fly";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:frenzy-plant}")){
                    text="Frenzy Plant";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=frenzy-plant";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fusion-bolt}")){
                    text="Fusion Bolt";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fusion-bolt";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:fusion-flare}")){
                    text="Fusion Flare";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=fusion-flare";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:gastro-acid}")){
                    text="Gastro Acid";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=gastro-acid";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:giga-impact}")){
                    text="Giga Impact";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=giga-impact";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:grass-pledge}")){
                    text="Grass Pledge";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=grass-pledge";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:guard-swap}")){
                    text="Guard Swap";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=guard-swap";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:guillotine}")){
                    text="Guillotine";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=guillotine";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:gust}")){
                    text="Gust";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=gust";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:heal-block}")){
                    text="Heal Block";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=heal-block";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:heart-swap}")){
                    text="Heart Swap";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=heart-swap";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:hidden-power}")){
                    text="Hidden Power";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=hidden-power";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:high-jump-kick}")){
                    text="High Jump Kick";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=high-jump-kick";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:horn-drill}")){
                    text="Horn Drill";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=horn-drill";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:hurricane}")){
                    text="Hurricane";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=hurricane";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:hydro-cannon}")){
                    text="Hydro Cannon";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=hydro-cannon";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:hydro-pump}")){
                    text="Hydro Pump";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=hydro-pump";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:hyper-beam}")){
                    text="Hyper Beam";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=hyper-beam";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:ice-ball}")){
                    text="Ice Ball";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=ice-ball";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:judgment}")){
                    text="Judgment";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=judgment";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:jump-kick}")){
                    text="Jump Kick";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=jump-kick";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:knock-off}")){
                    text="Knock Off";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=knock-off";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:leech-seed}")){
                    text="Leech Seed";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=leech-seed";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:lock-on}")){
                    text="Lock On";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=lock-on";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:magma-storm}")){
                    text="Magma Storm";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=magma-storm";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:magnitude}")){
                    text="Magnitude";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=magnitude";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:mean-look}")){
                    text="Mean Look";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=mean-look";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:memento}")){
                    text="Memento";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=memento";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:metal-burst}")){
                    text="Metal Burst";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=metal-burst";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:mind-reader}")){
                    text="Mind Reader";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=mind-reader";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:moonblast}")){
                    text="Moonblast";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=moonblast";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:natural-gift}")){
                    text="Natural Gift";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=natural-gift";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:nightmare}")){
                    text="Nightmare";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=nightmare";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:outrage}")){
                    text="Outrage";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=outrage";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:pain-split}")){
                    text="Pain Split";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=pain-split";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:petal-dance}")){
                    text="Petal Dance";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=petal-dance";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:phantom-force}")){
                    text="Phantom Force";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=phantom-force";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:pluck}")){
                    text="Pluck";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=pluck";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:power-swap}")){
                    text="Power Swap";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=power-swap";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:psychic-terrain}")){
                    text="Psychic Terrain";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=psychic-terrain";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:psycho-shift}")){
                    text="Psycho Shift";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=psycho-shift";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:pursuit}")){
                    text="Pursuit";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=pursuit";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:rapid-spin}")){
                    text="Rapid Spin";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=rapid-spin";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:razor-wind}")){
                    text="Razor Wind";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=razor-wind";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:roar-of-time}")){
                    text="Roar Of Time";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=roar-of-time";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:roar}")){
                    text="Roar";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=roar";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:rock-slide}")){
                    text="Rock Slide";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=rock-slide";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:rock-wrecker}")){
                    text="Rock Wrecker";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=rock-wrecker";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:rollout}")){
                    text="Rollout";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=rollout";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:round}")){
                    text="Round";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=round";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:sand-tomb}")){
                    text="Sand Tomb";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=sand-tomb";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:seed-bomb}")){
                    text="Seed Bomb";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=seed-bomb";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:shadow-force}")){
                    text="Shadow Force";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=shadow-force";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:sheer-cold}")){
                    text="Sheer Cold";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=sheer-cold";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:skull-bash}")){
                    text="Skull Bash";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=skull-bash";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:sky-attack}")){
                    text="Sky Attack";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=sky-attack";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:sky-drop}")){
                    text="Sky Drop";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=sky-drop";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:sky-uppercut}")){
                    text="Sky Uppercut";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=sky-uppercut";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:smack-down}")){
                    text="Smack Down";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=smack-down";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:smelling-salts}")){
                    text="Smelling Salts";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=smelling-salts";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:solar-beam}")){
                    text="Solar Beam";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=solar-beam";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:spider-web}")){
                    text="Spider Web";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=spider-web";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:steamroller}")){
                    text="Steamroller";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=steamroller";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:stomp}")){
                    text="Stomp";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=stomp";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:surf}")){
                    text="Surf";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=surf";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:swagger}")){
                    text="Swagger";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=swagger";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:switcheroo}")){
                    text="Switcheroo";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=switcheroo";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:taunt}")){
                    text="Taunt";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=taunt";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:techno-blast}")){
                    text="Techno Blast";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=techno-blast";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:teeter-dance}")){
                    text="Teeter Dance";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=teeter-dance";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:telekinesis}")){
                    text="Telekinesis";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=telekinesis";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:thief}")){
                    text="Thief";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=thief";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:thrash}")){
                    text="Thrash";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=thrash";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:thunder-wave}")){
                    text="Thunder Wave";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=thunder-wave";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:thunderbolt}")){
                    text="Thunderbolt";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=thunderbolt";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:thunder}")){
                    text="Thunder";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=thunder";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:torment}")){
                    text="Torment";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=torment";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:tri-attack}")){
                    text="Tri Attack";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=tri-attack";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:trick}")){
                    text="Trick";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=trick";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:twister}")){
                    text="Twister";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=twister";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:u-turn}")){
                    text="U Turn";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=u-turn";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:uproar}")){
                    text="Uproar";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=uproar";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:wake-up-slap}")){
                    text="Wake Up Slap";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=wake-up-slap";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:water-pledge}")){
                    text="Water Pledge";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=water-pledge";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:weather-ball}")){
                    text="Weather Ball";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=weather-ball";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:whirlpool}")){
                    text="Whirlpool";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=whirlpool";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:whirlwind}")){
                    text="Whirlwind";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=whirlwind";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:worry-seed}")){
                    text="Worry Seed";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=worry-seed";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:wrap}")){
                    text="Wrap";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=wrap";
                    hex_color="-";}
                else if (splitpart.equalsIgnoreCase("[]{move:yawn}")){
                    text="Yawn";
                    icon="-";
                    is_link="TRUE";
                    link_target="/move.xhtml?move=yawn";
                    hex_color="-";}



                else {
                    text = splitpart;
                    icon = "-";
                    is_link = "FALSE";
                    link_target = "-";
                    hex_color = "-";
                }

                text = text.replace(";", ",");

                System.out.println(entry.getId() + ";" + id + ";" + text + ";" + icon + ";" + is_link + ";" + link_target + ";" + hex_color);
            }
        });
    }
}
