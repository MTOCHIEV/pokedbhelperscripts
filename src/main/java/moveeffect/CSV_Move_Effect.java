package pokedbutils.moveeffect;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"id", "effect"})
public class CSV_Move_Effect {

    private String effect;
    private int id;
}