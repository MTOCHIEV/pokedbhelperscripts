package pokedbutils.moveeffect;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

public class Java_Writer {

    public static void main(String[] args) {
        List<String> moves = new ArrayList<>();
        moves.add("[]{move:destiny-bond}");
        moves.add("[]{mechanic:flinch}");
moves.add("[]{mechanic:stage}");
moves.add("[]{move:absorb}");
moves.add("[]{move:attract}");
moves.add("[]{move:avalanche}");
moves.add("[]{move:bind}");
moves.add("[]{move:blast-burn}");
moves.add("[]{move:blizzard}");
moves.add("[]{move:block}");
moves.add("[]{move:bounce}");
moves.add("[]{move:brick-break}");
moves.add("[]{move:bug-bite}");
moves.add("[]{move:chatter}");
moves.add("[]{move:circle-throw}");
moves.add("[]{move:clamp}");
moves.add("[]{move:covet}");
moves.add("[]{move:defog}");
moves.add("[]{move:dig}");
moves.add("[]{move:disable}");
moves.add("[]{move:dive}");
moves.add("[]{move:dragon-tail}");
moves.add("[]{move:dream-eater}");
moves.add("[]{move:earthquake}");
moves.add("[]{move:embargo}");
moves.add("[]{move:endeavor}");
moves.add("[]{move:energy-ball}");
moves.add("[]{move:feint}");
moves.add("[]{move:fire-pledge}");
moves.add("[]{move:fire-spin}");
moves.add("[]{move:fissure}");
moves.add("[]{move:flatter}");
moves.add("[]{move:fling}");
moves.add("[]{move:fly}");
moves.add("[]{move:frenzy-plant}");
moves.add("[]{move:fusion-bolt}");
moves.add("[]{move:fusion-flare}");
moves.add("[]{move:gastro-acid}");
moves.add("[]{move:giga-impact}");
moves.add("[]{move:grass-pledge}");
moves.add("[]{move:guard-swap}");
moves.add("[]{move:guillotine}");
moves.add("[]{move:gust}");
moves.add("[]{move:heal-block}");
moves.add("[]{move:heart-swap}");
moves.add("[]{move:hidden-power}");
moves.add("[]{move:high-jump-kick}");
moves.add("[]{move:horn-drill}");
moves.add("[]{move:hurricane}");
moves.add("[]{move:hydro-cannon}");
moves.add("[]{move:hydro-pump}");
moves.add("[]{move:hyper-beam}");
moves.add("[]{move:ice-ball}");
moves.add("[]{move:judgment}");
moves.add("[]{move:jump-kick}");
moves.add("[]{move:knock-off}");
moves.add("[]{move:leech-seed}");
moves.add("[]{move:lock-on}");
moves.add("[]{move:magma-storm}");
moves.add("[]{move:magnitude}");
moves.add("[]{move:mean-look}");
moves.add("[]{move:memento}");
moves.add("[]{move:metal-burst}");
moves.add("[]{move:mind-reader}");
moves.add("[]{move:moonblast}");
moves.add("[]{move:natural-gift}");
moves.add("[]{move:nightmare}");
moves.add("[]{move:outrage}");
moves.add("[]{move:pain-split}");
moves.add("[]{move:petal-dance}");
moves.add("[]{move:phantom-force}");
moves.add("[]{move:pluck}");
moves.add("[]{move:power-swap}");
moves.add("[]{move:psychic-terrain}");
moves.add("[]{move:psycho-shift}");
moves.add("[]{move:pursuit}");
moves.add("[]{move:rapid-spin}");
moves.add("[]{move:razor-wind}");
moves.add("[]{move:roar-of-time}");
moves.add("[]{move:roar}");
moves.add("[]{move:rock-slide}");
moves.add("[]{move:rock-wrecker}");
moves.add("[]{move:rollout}");
moves.add("[]{move:round}");
moves.add("[]{move:sand-tomb}");
moves.add("[]{move:seed-bomb}");
moves.add("[]{move:shadow-force}");
moves.add("[]{move:sheer-cold}");
moves.add("[]{move:skull-bash}");
moves.add("[]{move:sky-attack}");
moves.add("[]{move:sky-drop}");
moves.add("[]{move:sky-uppercut}");
moves.add("[]{move:smack-down}");
moves.add("[]{move:smelling-salts}");
moves.add("[]{move:solar-beam}");
moves.add("[]{move:spider-web}");
moves.add("[]{move:steamroller}");
moves.add("[]{move:stomp}");
moves.add("[]{move:surf}");
moves.add("[]{move:swagger}");
moves.add("[]{move:switcheroo}");
moves.add("[]{move:taunt}");
moves.add("[]{move:techno-blast}");
moves.add("[]{move:teeter-dance}");
moves.add("[]{move:telekinesis}");
moves.add("[]{move:thief}");
moves.add("[]{move:thrash}");
moves.add("[]{move:thunder-wave}");
moves.add("[]{move:thunderbolt}");
moves.add("[]{move:thunder}");
moves.add("[]{move:torment}");
moves.add("[]{move:tri-attack}");
moves.add("[]{move:trick}");
moves.add("[]{move:twister}");
moves.add("[]{move:u-turn}");
moves.add("[]{move:uproar}");
moves.add("[]{move:wake-up-slap}");
moves.add("[]{move:water-pledge}");
moves.add("[]{move:weather-ball}");
moves.add("[]{move:whirlpool}");
moves.add("[]{move:whirlwind}");
moves.add("[]{move:worry-seed}");
moves.add("[]{move:wrap}");
moves.add("[]{move:yawn}");

        moves.forEach(entry -> {

            String move_name = entry.replace("[]{move:","").replace("}","");
            String move_name_capitalized = WordUtils.capitalize(move_name.replace("-", " "));

            System.out.println(
                    "else if " + "(splitpart.equalsIgnoreCase("+"\""+entry+"\""+")){\n" +
                               "text="+"\""+move_name_capitalized+"\""+";"+"\n"+
                    "icon=\"-\""+";"+"\n"+
                    "is_link=\"TRUE\""+";"+"\n"+
                    "link_target=\""+"/move.xhtml?move="+move_name+"\""+";\n"+
                    "hex_color=\"-\";"+
                    "}"
                    );
        });
    }
}
