package pokedbutils.moveeffect_longeffect;

import org.apache.commons.text.WordUtils;
import pokedbutils.move_conversion.Utils;

import java.util.List;

public class App {
    public static void main(String[] args) {
        List<CSV_Move_Effect_Long_Effect> csv_ability_proses = Utils.convert_csv_to_entry_list(CSV_Move_Effect_Long_Effect.class, "move_effect_long_effect.csv");

        int old_id = csv_ability_proses.get(0).getMove_effect_id();
        boolean has_changed = false;
        int prose_order = 0;

        for (CSV_Move_Effect_Long_Effect entry : csv_ability_proses) {
            int current_id = entry.getMove_effect_id();
            if (old_id != current_id) {
                old_id = current_id;
                prose_order = 0;
            }
            String text = entry.getEffect_part();
            String icon = entry.getIcon();
            String is_link = entry.getIs_link();
            String link_target = entry.getLink_target();
            String hex_color = entry.getHex_color();


            text = WordUtils.wrap(text, 30);
            String[] splitparts = text.split("\r\n");
            for (String splitpart : splitparts) {
                prose_order++;
                System.out.println(
                        current_id + ";" + prose_order + ";" + splitpart +";" + icon + ";" + is_link + ";" + link_target + ";" + hex_color);
                if (!icon.equalsIgnoreCase("-")) icon = "-";
                if (is_link.equalsIgnoreCase("TRUE")) is_link = "FALSE";
                if (!link_target.equalsIgnoreCase("-")) link_target = "-";
                if (!hex_color.equalsIgnoreCase("-")) hex_color = "-";
            }
        }
    }
}
