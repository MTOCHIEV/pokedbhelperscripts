package pokedbutils.moveeffect_longeffect;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"move_effect_id", "prose_order", "effect_part", "icon", "is_link", "link_target", "hex_color"})
public class CSV_Move_Effect_Long_Effect {

    private String effect_part;
    private String hex_color;
    private String icon;
    private String is_link;
    private String link_target;
    private int move_effect_id;
    private int prose_order;
}