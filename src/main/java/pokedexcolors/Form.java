package pokedbutils.pokedexcolors;

import lombok.Data;

@Data
public class Form {
    public String formnr;
    public String absolute_id;
    public String germanName;
    public String color;
}
