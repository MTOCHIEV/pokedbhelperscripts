package pokedbutils.pokedexcolors;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MissingPokedexColors {
    public static void main(String[] args) {
        List<List<String>> colors = convert_csv_to_entry_list("Book1.csv");
        List<List<String>> forms = convert_csv_to_entry_list("pokemon_forms.csv");
        List<List<String>> names = convert_csv_to_entry_list("pokemon_names.csv");

        List<Name> namesList = names.stream().map(entry -> {
            Name name = new Name();
            name.setAbsolutenr(entry.get(0));
            name.setGermanName(entry.get(2));
            return name;
        }).toList();

        List<PokedexColor> colorsList = colors.stream().map(entry -> {
            PokedexColor pokedexColor = new PokedexColor();
            pokedexColor.setColor(entry.get(0));
            pokedexColor.setGerman_name(entry.get(1));
            pokedexColor.setForm_info(entry.get(2));
            for (Name name : namesList) {
                if (name.germanName.equalsIgnoreCase(entry.get(1))) {
                    pokedexColor.setAbsolute_pokemon_id(name.getAbsolutenr());
                    break;
                }
            }
            return pokedexColor;
        }).toList();

        List<String> germanNames = colors.stream().map(entry -> entry.get(1)).toList();


        List<Form> formsList = new ArrayList<>(forms.stream().map(entry -> {
            Form form = new Form();
            form.setFormnr(entry.get(0));
            form.setAbsolute_id(entry.get(1));
            List<PokedexColor> eligible = colorsList.stream().filter(pokedexColor -> {
                int form_as_int = Integer.parseInt(form.absolute_id);
                int color_as_int = 0;
                if (!StringUtils.isEmpty(pokedexColor.absolute_pokemon_id)) color_as_int = Integer.parseInt(pokedexColor.absolute_pokemon_id);
                return form_as_int == color_as_int;
            }).toList();
            if (eligible.size() == 1) {
                form.setColor(eligible.get(0).getColor());
            } else {
                for (PokedexColor pokedexColor : eligible) {
                    if (Collections.frequency(germanNames, pokedexColor.getGerman_name()) != 1 && pokedexColor.absolute_pokemon_id == null) {
                        form.setGermanName(pokedexColor.german_name + " " + pokedexColor.getForm_info());
                        break;
                    }
                }
            }

            return form;
        }).toList());

        formsList.sort((o1, o2) -> {
            int o1_as_int = Integer.parseInt(o1.getFormnr());
            int o2_as_int = Integer.parseInt(o2.getFormnr());
            return Integer.compare(o1_as_int, o2_as_int);
        });

        formsList.forEach(entry -> {
            if (!StringUtils.isEmpty(entry.getColor())) System.out.println(entry.formnr + ";" + entry.color);
            else System.out.println(entry.formnr + ";" + entry.germanName);
        });
    }


    public static List<List<String>> convert_csv_to_entry_list(String csvPath) {
        URL csvURL = MissingPokedexColors.class.getClassLoader().getResource(csvPath);
        List<List<String>> records = new ArrayList<>();
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvURL.toURI()));
        } catch (IOException | URISyntaxException e) {
            System.out.println("COULD NOT READ CSV FILE");
            throw new RuntimeException(e);
        }
        CSVParser parser = new CSVParserBuilder().withSeparator(';').withIgnoreQuotations(true).build();
        CSVReader csv_reader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();
        String[] values;
        while (true) {
            try {
                if ((values = csv_reader.readNext()) == null) break;
            } catch (IOException | CsvValidationException e) {
                System.out.println("COULD NOT READ NEXT CSV LINE");
                throw new RuntimeException(e);
            }
            records.add(Arrays.asList(values));
        }
        return records;
    }

    public static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}