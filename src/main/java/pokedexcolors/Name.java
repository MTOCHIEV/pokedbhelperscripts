package pokedbutils.pokedexcolors;

import lombok.Data;

@Data
public class Name {
    public String absolutenr;
    public String germanName;
}
