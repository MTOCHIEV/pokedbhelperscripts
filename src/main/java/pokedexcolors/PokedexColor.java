package pokedbutils.pokedexcolors;

import lombok.Data;

@Data
public class PokedexColor {
    public String color;
    public String absolute_pokemon_id;
    public String german_name;
    public String form_info;
}
