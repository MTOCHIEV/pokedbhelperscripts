package pokedbutils.shinyimages;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        File folder = new File("C:\\workspaces\\PokeDB\\src\\main\\resources\\pokemon_data\\assets\\pokemon_main_image_shiny");
        printNonSquareImages(folder);
    }

    public static void printNonSquareImages(File folder) {
        File[] files = folder.listFiles();

        for (File file : files) {
            if (file.isFile() && (file.getName().toLowerCase().endsWith(".jpg") || file.getName().toLowerCase().endsWith(".png") || file.getName().toLowerCase().endsWith(".bmp") ||
                                  file.getName().toLowerCase().endsWith(".gif"))) {

                try {
                    BufferedImage image = ImageIO.read(file);
                    if (image.getHeight() != image.getWidth()) {
                        System.out.println(file.getName() + " is not a square image.");
                    }
                } catch (IOException e) {
                    System.out.println("Error reading image " + file.getName());
                }
            } else if (file.isDirectory()) {
                printNonSquareImages(file);
            }
        }
    }
}
