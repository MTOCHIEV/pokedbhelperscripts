package pokedbutils.type_conversion;

import lombok.SneakyThrows;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {

        List<Type_New> types_new = Utils.convert_csv_to_entry_list(Type_New.class, "type_conversion/types_new.csv");
        List<Type_Old> types_old = Utils.convert_csv_to_entry_list(Type_Old.class, "type_conversion/types_old.csv");
        List<Weakness> weaknesses = pokedbutils.move_conversion.Utils.convert_csv_to_entry_list(Weakness.class, "type_conversion/type_weakness.csv");


        for (Weakness weakness : weaknesses) {
            if (!weakness.getDefense_type_1().equalsIgnoreCase("-")) {
                String name1_in_old = types_old.stream().filter(typeOld -> typeOld.getId() == Integer.parseInt(weakness.getDefense_type_1())).findFirst().get().getIdentifier();
                String id1_in_new = String.valueOf(types_new.stream().filter(typeNew -> typeNew.getIdentifier().equalsIgnoreCase(name1_in_old)).findFirst().get().getId());
                weakness.setDefense_type_1(id1_in_new);
            }
            if (!weakness.getDefense_type_2().equalsIgnoreCase("-")) {
                String name1_in_old = types_old.stream().filter(typeOld -> typeOld.getId() == Integer.parseInt(weakness.getDefense_type_2())).findFirst().get().getIdentifier();
                String id1_in_new = String.valueOf(types_new.stream().filter(typeNew -> typeNew.getIdentifier().equalsIgnoreCase(name1_in_old)).findFirst().get().getId());
                weakness.setDefense_type_2(id1_in_new);
            }
        }

        writeToCsv(weaknesses, "defense_type_1;defense_type_2");
    }

    @SneakyThrows
    public static <T> void writeToCsv(List<T> sampleList, String header) {
        PrintWriter writer = new PrintWriter("weakness_new.csv");
        writer.println(header);

        for (T sample : sampleList) {
            writer.println(sample);
        }
        writer.close();
    }
}
