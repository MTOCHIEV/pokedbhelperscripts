package pokedbutils.type_conversion;

import java.util.List;

public class App2 {
    public static void main(String[] args) {
        List<Type_CSV> types = Utils.convert_csv_to_entry_list(Type_CSV.class, "type_weakness.csv");

        String s = null;
        for (Type_CSV type : types) {
            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getNormal() + ";" + "Normal";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getFire() + ";" + "Fire";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getWater() + ";" + "Water";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getElectric() + ";" + "Electric";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getGrass() + ";" + "Grass";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getIce() + ";" + "Ice";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getFighting() + ";" + "Fighting";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getPoison() + ";" + "Poison";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getGround() + ";" + "Ground";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getFlying() + ";" + "Flying";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getPsychic() + ";" + "Psychic";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getBug() + ";" + "Bug";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getRock() + ";" + "Rock";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getGhost() + ";" + "Ghost";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getDragon() + ";" + "Dragon";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getDark() + ";" + "Dark";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getSteel() + ";" + "Steel";
            System.out.println(s);

            s = type.getDefense_type_1() + ";" + type.getDefense_type_2() + ";" + type.getFairy() + ";" + "Fairy";
            System.out.println(s);

        }
    }
}
