package pokedbutils.type_conversion;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"defense_type_1", "defense_type_2", "normal", "fire", "water", "electric", "grass", "ice", "fighting", "poison", "ground", "flying", "psychic", "bug", "rock", "ghost", "dragon", "dark", "steel", "fairy"})
public class Type_CSV {

    private String bug;
    private String dark;
    private String defense_type_1;
    private String defense_type_2;
    private String dragon;
    private String electric;
    private String fairy;
    private String fighting;
    private String fire;
    private String flying;
    private String ghost;
    private String grass;
    private String ground;
    private String ice;
    private String normal;
    private String poison;
    private String psychic;
    private String rock;
    private String steel;
    private String water;
}

