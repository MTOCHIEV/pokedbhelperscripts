package pokedbutils.type_conversion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"defense_type_1", "defense_type_2"})
public class Weakness {

    private String defense_type_1;
    private String defense_type_2;

    @Override
    public String toString() {
        StringBuilder dataBuilder = new StringBuilder();
        appendFieldValue(dataBuilder, defense_type_1);
        appendFieldValue(dataBuilder, defense_type_2);
        return dataBuilder.toString();
    }

    private void appendFieldValue(StringBuilder dataBuilder, String fieldValue) {
        if (fieldValue != null) {
            dataBuilder.append(fieldValue).append(";");
        } else {
            dataBuilder.append(";");
        }
    }
}

